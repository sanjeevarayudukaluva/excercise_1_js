function findSumOfSalary(data) {
  let  sumOfSalary = 0;
  if (Array.isArray(data) && data.length !== 0) {
    for (let index = 0; index < data.length; index++) {
        const salary = parseFloat(data[index].salary.replace("$", ""));
            const correctedSalary = salary * 10000;
      sumOfSalary += correctedSalary;
    }
    return sumOfSalary;
  } else {
    return null;
  }
}
module.exports = { findSumOfSalary };
