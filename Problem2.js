function convertSalaryToNumberFormate(data) {
  const convertedData = [];
  if (Array.isArray(data) && data.length !== 0) {
    for (let index = 0; index < data.length; index++) {
      const salary = parseFloat(data[index].salary.replace("$", ""));
      const convertedSalary = salary * 10000;
      data[index].salary = convertedSalary;
      convertedData.push(data[index]);
    }
  }
  return convertedData;
}
module.exports = { convertSalaryToNumberFormate };
