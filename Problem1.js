function findingAllWebDevelopers(data) {
  const webDevelopersData = [];
  if (Array.isArray(data) && data.length !== 0) {
    for (let index = 0; index < data.length; index++) {
      if (data[index].job.startsWith("Web Developer")) {
        webDevelopersData.push(data[index]);
      }
    }
  }
  return webDevelopersData;
}
module.exports={findingAllWebDevelopers};