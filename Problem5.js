function sumOfAllSalaryBasedOnCountry(data) {
    const salaryByCountry = {};
  if (data != null && Array.isArray(data) && data.length !== 0) {
    for (let index = 0; index < data.length; index++) {
        let salary = parseFloat(data[index].salary.replace("$", ""));
        let country = data[index].location;
    
        if (!isNaN(salary)) {
          if (salaryByCountry[country]) {
            salaryByCountry[country] += salary;
          } else {
            salaryByCountry[country] = salary;
          }
        } else {
          console.log("Error: Invalid salary format.");
          return null;
        }
      }
    
      
    }
    return salaryByCountry;
}

module.exports={sumOfAllSalaryBasedOnCountry};