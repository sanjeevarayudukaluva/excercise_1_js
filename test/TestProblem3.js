// Convert all the salary values into proper numbers instead of strings.

const employeesData = require("../js_drill_2.cjs");
const convertingSalary = require("../Problem3.js");

try {
  const convertSalary =
    convertingSalary.salaryIsFactorOf10000(
      employeesData.employees
    );
  if (
    convertingSalary === null ||
    convertingSalary.length === 0 ||
    convertingSalary === undefined
  ) {
    throw new Error("data is empty or null or undefined");
  }
  console.log(`${JSON.stringify(convertSalary)}`);
} catch (error) {
  console.log(error);
}
