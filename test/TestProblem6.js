const data = require("../js_drill_2.cjs");
const averageSalaryCountry = require("../Problem6.js");
try {
  const avgSalaryBasedOnCountry = averageSalaryCountry.getAverageSalaryBasedOnCountry(
    data.employees
  );
  if (avgSalaryBasedOnCountry === null || avgSalaryBasedOnCountry === undefined) {
    throw new Error("Data null or undefined");
  }
  console.log(`${JSON.stringify(avgSalaryBasedOnCountry)}`);
} catch (error) {
  console.log(error);
}
