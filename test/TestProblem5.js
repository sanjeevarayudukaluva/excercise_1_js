const data = require("../js_drill_2.cjs");
const sumSalaryCountry = require("../Problem5.js");
try {
  const sumOfSalary = sumSalaryCountry.sumOfAllSalaryBasedOnCountry(
    data.employees
  );
  if (sumOfSalary === null || sumOfSalary === undefined) {
    throw new Error("Data null or undefined");
  }
  console.log(`${JSON.stringify(sumOfSalary)}`);
} catch (error) {
  console.log(error);
}
