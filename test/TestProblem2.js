// Convert all the salary values into proper numbers instead of strings.

const employeesData = require("../js_drill_2.cjs");
const convertingSalaryToNumber = require("../Problem2.js");

try {
  const convertingSalary =
    convertingSalaryToNumber.convertSalaryToNumberFormate(
      employeesData.employees
    );
  if (
    convertingSalary === null ||
    convertingSalary.length === 0 ||
    convertingSalary === undefined
  ) {
    throw new Error("data is empty or null or undefined");
  }
  console.log(`${JSON.stringify(convertingSalary)}`);
} catch (error) {
  console.log(error);
}
