const data = require("../js_drill_2.cjs");
const sumSalary = require("../Problem4.js");
try {
  const sumOfSalary = sumSalary.findSumOfSalary(data.employees);
  if (sumOfSalary === null || sumOfSalary === undefined) {
    throw new Error("Data null or undefined");
  }
  console.log(`sum of all employee salary  = ${sumOfSalary}`);
} catch (error) {
  console.log(error);
}
