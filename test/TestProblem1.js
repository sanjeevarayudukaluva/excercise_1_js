// Find all Web Developers. ( It could be Web Developer III or Web Developer II or anything else )

const employeesData = require("../js_drill_2.cjs");
// console.log(`${JSON.stringify(employeesData.employees)}`);
const findWebDevelopers = require("../Problem1.js");

try {
  const webDevelopers = findWebDevelopers.findingAllWebDevelopers(
    employeesData.employees
  );
  if (
    webDevelopers === null ||
    webDevelopers.length === 0 ||
    webDevelopers === undefined
  ) {
    throw new Error("data is null or empty or undefined");
  }
  console.log(`${JSON.stringify(webDevelopers)}`);
} catch (error) {
  console.log(error);
}
