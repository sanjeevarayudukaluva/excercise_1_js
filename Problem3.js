function salaryIsFactorOf10000(data) {
    const convertedData = [];
    if (Array.isArray(data) && data.length !== 0) {
        for (let index = 0; index < data.length; index++) {
            const salary = parseFloat(data[index].salary.replace("$", ""));
            const correctedSalary = salary * 10000;
            data[index].corrected_salary = correctedSalary;
            convertedData.push(data[index]);
        }
        return convertedData;
    }
    return [];
}

module.exports = { salaryIsFactorOf10000 };
